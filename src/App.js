import './App.css';
import Box from './Box/Box.js';

const hexValues = '0123456789ABCDEF';

const getNewColor = () => {
  return Array(6).fill(0).reduce((prev, cur) => {
    return prev + hexValues[Math.floor(Math.random() * hexValues.length)]}, '#');
};

let colors = Array(10).fill(0).map(getNewColor);

function App() {
  return (
    colors.map((item, i) => <Box  color={item} key={Math.random()} />)
  );
}

export default App;
