import { Component } from "react";

class Box extends Component {
    circleShape = false;
    constructor(props) {
        super(props);
        this.state = {
            color: this.props.color,
            box: true
        };
    }

    getNColor = () => {
        const hexValues = '0123456789ABCDEF';
        return Array(6).fill(0).reduce((prev, cur) => {
            return prev + hexValues[Math.floor(Math.random() * hexValues.length)]
        }, '#');
    };


    render() {
        return (
            <div className="shape-color">
                <div className={this.state.box ? 'box common-box-circle-shape' : 'circle common-box-circle-shape'} style={{ backgroundColor: this.state.color }}>
                    <button type="button" onClick={() => {
                        const nColor = this.getNColor();
                        this.setState({color: nColor});
                    }} className="box-color-shape">Change color</button>
                    
                    <button className="box-color-shape" onClick={() => {
                        this.circleShape = !this.circleShape;
                        this.setState({box: !this.state.box})
                        }}>{this.circleShape ? 'Change to Box' : 'Change to Circle'}</button>
                </div>
            </div>
        );
    };
}



export default Box;